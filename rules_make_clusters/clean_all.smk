configfile: "config.yaml"

generated_data_dir = config["generated_data_dir"]
output_dir = config["output_dir"]
tax_list_file = config['tax_list_file']

rule clean:
    shell:
        """
        rm -f {tax_list_file}
        rm -f {generated_data_dir}fetched/*.aa
        rm -f {generated_data_dir}fetched/*.nuc
        rm -f {generated_data_dir}fetched/prot.fasta
        rm -f {generated_data_dir}fetched/prot_no_space.fasta
        rm -f {generated_data_dir}prot.blast.out
        rm -f {generated_data_dir}prot.silix.out
        rm -f {generated_data_dir}prot_clusters/protein_list.txt
        rm -f {generated_data_dir}prot_clusters/cl*.fa
        rm -f {generated_data_dir}prot_clusters/*.hmm
        rm -f {generated_data_dir}prot_clusters/*.aln.fa
        rm -f {generated_data_dir}prot_clusters/*.hmmgen.sto
        rm -f {generated_data_dir}prot_clusters/*.hmmgen.fa
        rm -f {generated_data_dir}prot_clusters/*.hmmgen.fa.unaln
        rm -f {generated_data_dir}prot_clusters/*.hmmgen.fa.dist
        rm -f {output_dir}*.hmmgen_and_init.aln.fa
        rm -f {output_dir}*.hmmgen_and_init.aln.nuc.fa
        rm -f {output_dir}*.hmmgen_and_init.aa.fasttree
        if [ -d "{generated_data_dir}fetched/" ]; then rmdir --ignore-fail-on-non-empty {generated_data_dir}fetched/; fi
        if [ -d "{generated_data_dir}prot_clusters/" ]; then rmdir --ignore-fail-on-non-empty {generated_data_dir}prot_clusters/; fi
        if [ -d "{generated_data_dir}list_taxon/" ]; then rmdir --ignore-fail-on-non-empty {generated_data_dir}list_taxon/; fi
        if [ -d "{output_dir}" ]; then rmdir --ignore-fail-on-non-empty {output_dir}; fi
        if [ -d "{generated_data_dir}" ]; then rmdir --ignore-fail-on-non-empty {generated_data_dir}; fi
        """