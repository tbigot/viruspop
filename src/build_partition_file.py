#!/usr/bin/env python3
import argparse
import itertools
import operator

# Reads the rate file and sorts the loci based on rate group
def iqtree_rate_partition(iqtree_rate_file, nexus_output_file, model):
    reader = (l.split()[0:4] for l in open(iqtree_rate_file) if l.split()[0].isdigit())
    sorted_by_partition = sorted(reader, key=operator.itemgetter(2))
    partitions = itertools.groupby(sorted_by_partition, key=operator.itemgetter(2))
    rates = [next(partition[1])[3] for partition in itertools.groupby(sorted_by_partition, key=operator.itemgetter(2))]
    write_nexus_file(partitions, 0, nexus_output_file, model, rates)


# Parses the rate data as a Nexus file
def write_nexus_file(partitions, position_index, nexus_output_file, model, rate=False):
    model_list = []
    with open(nexus_output_file, 'w') as output:
        output.write("#nexus\nbegin sets;\n")
        for idx, p in enumerate(partitions):
            output.write(f"    charset part{idx + 1} =")
            sorted_positions = sorted((int(l[position_index]) for l in p[1]))
            block_start = sorted_positions[0]
            print(f"PARTITION {idx + 1} with {len(sorted_positions)} loci.")
            for i in range(1, len(sorted_positions)):
                if sorted_positions[i] > sorted_positions[i - 1] + 1:
                    output.write(f" {block_start}-{sorted_positions[i-1]}")
                    block_start = sorted_positions[i]
            output.write(f" {block_start}-{sorted_positions[-1]};\n")

            if rate:
                model_list.append(f"{model}:part{idx + 1}" + "{" + rate[idx] + "}")
            else:
                model_list.append(f"{model}:part{idx + 1}")
        output.write("    charpartition mine = " + ', '.join(model_list) + ";\nend;\n")


if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description="Extracts data from BMGE or IQtree rate file and constructs a nexus "
                                                 "partition file for IQtree alisim")
    parser.add_argument("input_file", help="input file")
    parser.add_argument("output_nexus_file", help="output nexus file")
    parser.add_argument("substitution_model", help="Substitution model name for nexus file")
    args = parser.parse_args()
    ## \endcond
    iqtree_rate_partition(args.input_file, args.output_nexus_file, args.substitution_model)
