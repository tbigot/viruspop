#!/usr/bin/env python3
import argparse
import multiprocessing
import os
import subprocess
import sys
from random import random

sys.path.append(os.environ.get('current_dir'))
from utils_fasta import new_read_fasta_file, write_sequence_in_open_file

def worker_simulation(args):
    directory, nexus_file, dist, dist_init, seed, input_file, identifier, cluster_name = args
    temp_tree_file_name = directory + "/tmptree_" + cluster_name + "_" + str(multiprocessing.current_process().pid)
    temp_sim_file_name = directory + "/tmpsim_" + cluster_name + "_" + str(multiprocessing.current_process().pid)
    command_args = ['iqtree', '--alisim', temp_sim_file_name, '-p', nexus_file, '-t', temp_tree_file_name, '-af',
                    'fasta', '-nt', '1']
    if seed:
        command_args += ['-seed', str(seed)]

    with open(temp_tree_file_name, 'w') as tree_file:
        tree_file.write(f"(B:{dist},C:{dist});")
    subprocess.run(command_args + ['--root-seq', f"{input_file},{identifier}"], capture_output=True, text=True)
    new_seq = open(temp_sim_file_name + '.fa').read().split("\n")[1]
    new_id = f"{identifier}_dist{dist}" if not dist_init else f"{'_'.join(identifier.split('_')[0:-1])}_dist{dist}"
    return new_id, new_seq



def simulate(input_file, dist_init, nexus_file, output_file, n_proc, n_dist_var = 3, seed=False):
    cluster_name = os.path.basename(nexus_file).split(".")[0]
    if not n_proc:
        n_proc = 1
    print("Running simulation for " + cluster_name + " on " + str(n_proc) + " processors")
    directory = os.path.dirname(nexus_file)
    parameters = []
    for idx, (identifier, _) in enumerate(new_read_fasta_file(input_file)):
        if not dist_init:
            dist = float(identifier.split("_")[-1])
            dist = 0.01 if dist < 0.01 else dist
            if n_dist_var:
                min, max = dist / 2, dist * 2
                dist_sim = [round(min + (max-min)/(n_dist_var - 1) * i, 3) for i
                            in range(n_dist_var)]
            else:
                dist_sim = [dist]
        else:
            dist_sim = [float(dist_init)]
        for dist_i in dist_sim:
            parameters.append((directory, nexus_file, dist_i, dist_init, seed, input_file, identifier, cluster_name))
            if seed:
                seed += 1

    with multiprocessing.get_context("fork").Pool(n_proc) as pool:
        with open(output_file, "w") as output:
            for new_id, new_seq in pool.imap_unordered(worker_simulation, parameters):
                write_sequence_in_open_file((new_id, new_seq), output)

    for file in os.listdir(directory):
        if cluster_name in file and file.startswith("tmp"):
            os.remove(os.path.join(directory, file))




if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description="Simulate sequences at a distance 'dist' of input sequences based on "
                                                 "a given substitution model with partitions")
    parser.add_argument("input_fasta_file", help="Input fasta file")
    parser.add_argument("nexus_file", help="Partition description file (nexus format)")
    parser.add_argument("output_file", help="Output file name")
    parser.add_argument("n_proc", help="Number of processors")
    parser.add_argument("-d", "--dist", help="Distance for simulation. If not given, distances must be at the end of "
                                             "the ancestral sequence identifiers (with \"_\" as separator)")
    parser.add_argument("-v", "--dist_variation_number", help="If given, multiple dist will be generated equaly spread between dist/2 and 2*dist")
    parser.add_argument("-s", "--seed", help="Random seed")
    args = parser.parse_args()
    ## \endcond
    if args.seed:# for deletion
        random.seed(args.seed)
    simulate(args.input_fasta_file, args.dist, args.nexus_file, args.output_file, int(args.n_proc), n_dist_var=int(args.dist_variation_number), seed=args.seed)