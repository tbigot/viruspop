import argparse
import os
import sys

sys.path.append(os.environ.get('current_dir'))
from translation import complement, check_translation_simple
from utils_fasta import new_read_fasta_file, write_sequence_in_open_file


def get_initial_nuc_sequences(directory, fasta_aa_file, output_file):
    nuc_file_name_and_aa_sequence = (
        (identifier, directory + identifier.split("|")[0] + ".nuc", sequence.replace("-", "")) for identifier, sequence
        in new_read_fasta_file(fasta_aa_file))
    identifier_nuc_and_aa_sequence = (
        (identifier, "".join([l.rstrip() for l in open(file) if not l.startswith(">")]), sequence_aa) for
        (identifier, file, sequence_aa) in nuc_file_name_and_aa_sequence)
    with open(output_file, 'w') as output:
        for id_aa, gen, seq_aa in identifier_nuc_and_aa_sequence:
            try:
                coord = id_aa.split("|")[2]
                if coord.startswith("complement"):
                    reverse = True
                    coord = coord[11:-1]
                else:
                    reverse = False
                seg = ([int(c) for c in s.split("..")] for s in coord.replace("join(", "").replace(")", "").split(","))
                seg_nuc = [gen[start - 1:stop] for start, stop in seg]
                nuc = "".join(reversed(complement("".join(seg_nuc).upper()))) if reverse else "".join(seg_nuc).upper()
                if check_translation_simple(nuc, seq_aa):
                    write_sequence_in_open_file((id_aa, nuc), output)
            except ValueError as e:
                pass  # example of problematic bound : complement(986..>1843)
            except Exception as e:
                print("Problem for finding nuc sequence of protein in file " + fasta_aa_file)
                print("with identifier : " + id_aa)
                print(e)


if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description="Simulate sequences at a distance 'dist' of input sequences based on "
                                                 "a given substitution model with partitions")
    parser.add_argument("directory", help="directory")
    parser.add_argument("fasta_aa_file", help="fasta_aa_file)")
    parser.add_argument("output_file", help="Output file name")
    args = parser.parse_args()
    ## \endcond
    get_initial_nuc_sequences(args.directory, args.fasta_aa_file, args.output_file)
