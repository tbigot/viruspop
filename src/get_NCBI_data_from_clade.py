import argparse
import operator
import random
import itertools

from Bio import Entrez
from ete3 import NCBITaxa


def find_sub_species_and_random_pick_data(clade, genome_2_download_per_species, file_output):

    def get_all_species(clade_id, ncbi):
        rank = ncbi.get_rank([clade_id])[int(clade_id)]
        if rank == 'species':
            return [clade_id]
        elif rank:
            try:
                sub_tree = ncbi.get_descendant_taxa(clade_id, collapse_subspecies=True, return_tree=True)
                if not isinstance(sub_tree, list):
                    return itertools.chain.from_iterable(get_all_species(clade.name, ncbi) for clade in sub_tree.children)
            except AttributeError as error:
                pass
        return []

    def NCBI_search_for_complete_species_genome(species_name):
        research = '"' + species_name + '"[Organism] AND "complete genome"[All Fields] NOT nearly[All Fields] '
        print('NCBI search with terms : "' + research)
        data = Entrez.read(Entrez.esearch(db="nucleotide", term=research, retmode="xml", RetMax="10000"))
        if data['Count'] != '0':
            print(f"---> done, {data['Count']} results")
            return species_name, data['IdList']
        else:
            print(f"---> done, no results")
            return species_name, None

    def make_unique(lst):
        seen = set()
        unique = [x for x in lst if x not in seen and not seen.add(x)]
        return unique

    def write_output_file(clade, species_definition_locus, file_output):
        with open(file_output, 'w') as output:
            output.write("#"*(len(clade) + 16) + f"\n######  {clade}  ######\n" + "#"*(len(clade) + 16) + "\n")
            for species, group in species_definition_locus:
                list_locus = list(group)
                output.write(f"\n##### {species} ({len(list_locus)})\n")
                for _, definition, locus in list_locus:
                    output.write(f"# {definition}\n")
                    output.write(f"{locus}\n")


    ncbi = NCBITaxa()
    Entrez.email = "eigtw510tyjrt403@gmail.com"
    cladeId = ncbi.get_name_translator([clade])[clade][0]
    list_species_id = list(get_all_species(cladeId, ncbi))
    dic_id_to_name_species = ncbi.get_taxid_translator(list_species_id)
    eSearch_results = (NCBI_search_for_complete_species_genome(dic_id_to_name_species[id]) for id in
                       dic_id_to_name_species)
    list_tax_ids = [(len(data), species, data) for (species, data) in eSearch_results if data]
    total = sum([nb for nb, _, _ in list_tax_ids])

    picked_genomes = {species: make_unique(random.choices(lst, k=genome_2_download_per_species)) for _, species, lst in
                      list_tax_ids}
    inverse_dictionary = {id_db: species for species in picked_genomes for id_db in picked_genomes[species]}
    all_ids = list(itertools.chain.from_iterable(picked_genomes[species] for species in picked_genomes))
    print('fetching data on NCBI "nucleotide" db')
    nucleotide_data = Entrez.read(Entrez.efetch(db="nucleotide", id=all_ids, retmode="xml"))
    print('---> done')
    id_and_data = (([id_gi.split('|')[-1] for id_gi in data['GBSeq_other-seqids'] if id_gi[0:2] == 'gi'], data) for data
                   in nucleotide_data)
    species_definition_locus = itertools.groupby(sorted(
        (inverse_dictionary[id_gi[0]], data['GBSeq_definition'], data['GBSeq_locus']) for id_gi, data in id_and_data),
                                                 operator.itemgetter(0))
    write_output_file(clade, species_definition_locus, file_output)



if __name__ == "__main__":

    ## \cond
    parser = argparse.ArgumentParser()
    parser.add_argument("clade", help="Clade name")
    parser.add_argument("nb_target", help="Target number of genome to download per species found")
    parser.add_argument("file_output", help="Output file")
    parser.add_argument("-s", "--seed", help="Random seed")
    args = parser.parse_args()
    ## \endcond
    if args.seed:
        random.seed(args.seed)
    find_sub_species_and_random_pick_data(args.clade, int(args.nb_target), args.file_output)
