import argparse
from argparse import HelpFormatter


def check_positive(value):
    if not value.isdigit() or not int(value) > 0:
        raise argparse.ArgumentTypeError("%s must be a positive int value" % value)
    return int(value)


def check_is_list(value):
    try:
        for string in value.replace('"', "").split():
            float(string)
    except ValueError:
        raise argparse.ArgumentTypeError("Check that your distances are all floats in %s." % value)
    return value


def create_save_file(args):
    return "\n".join([key + "\t" + str(item) for key, item in vars(args) if not key.startswith("_")])


class MyFormatter(HelpFormatter):
    def _format_action_invocation(self, action):
        if not action.option_strings:
            default = self._get_default_metavar_for_positional(action)
            metavar, = self._metavar_formatter(action, default)(1)
            return metavar

        else:
            parts = []

            # if the Optional doesn't take a value, format is:
            #    -s, --long
            if action.nargs == 0:
                parts.extend(action.option_strings)

            # if the Optional takes a value, format is:
            #    -s ARGS, --long ARGS
            else:
                default = self._get_default_metavar_for_optional(action)
                args_string = self._format_args(action, default)
                for option_string in action.option_strings:
                    parts.append(option_string)

                return '%s <%s>' % (', '.join(parts), args_string)

            return ', '.join(parts)

    def _get_default_metavar_for_optional(self, action):
        return action.dest.upper()

    def _split_lines(self, text, width):
        return text.splitlines()

    def _fill_text(self, text, width, indent):
        return ''.join(line for line in text.splitlines(keepends=True))





