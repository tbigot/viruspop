import itertools
import sys, os
sys.path.append(os.environ.get('current_dir'))
from utils_fasta import new_read_fasta_file, write_sequence_in_open_file

aa_to_nt_codon_str = """
    *: TAA TAG TGA
    A: GCA GCC GCG GCT
    C: TGC TGT
    D: GAC GAT
    E: GAA GAG
    F: TTC TTT
    G: GGA GGC GGG GGT
    H: CAC CAT
    I: ATA ATC ATT
    K: AAA AAG
    L: CTA CTC CTG CTT TTA TTG
    M: ATG
    N: AAC AAT
    P: CCA CCC CCG CCT
    Q: CAA CAG
    R: AGA AGG CGA CGC CGG CGT
    S: AGC AGT TCA TCC TCG TCT
    T: ACA ACC ACG ACT
    V: GTA GTC GTG GTT
    W: TGG
    Y: TAC TAT
"""

aa_to_nt_codon = {
    (None if aa == "*" else aa): codon.split()
    for aa, codon in (
        l.strip().split(":") for l in aa_to_nt_codon_str.splitlines() if l
    )
}

ambiguous_nt_to_nt_str = """
    N: ACGT         # A or C or G or T )
    R: AG           # Purine (A or G)
    Y: TC           # Pyrimidine (T or C)
    K: GT           # Keto (G or T)
    M: AC           # Amino (A or C)
    S: GC           # Strong interaction (3 H bonds) (G or C)
    W: AT           # Weak interaction (2 H bonds) (A or T)
    B: CGT          # Not A (C or G or T)
    D: AGT          # Not C (A or G or T)
    H: ACT          # Not G (A or C or T)
    V: ACG          # Not T (A or C or G)
"""

ambiguous_nt_to_nt = {
    amb: set(nts.strip())
    for (amb, nts) in (
        l.split("#")[0].strip().split(":")
        for l in ambiguous_nt_to_nt_str.splitlines()
        if l
    )
}

codon_to_aa = {codon: aa for aa in aa_to_nt_codon for codon in aa_to_nt_codon[aa]}

codontable_without_ambiguous = {
    codon: aa for (aa, codons) in aa_to_nt_codon.items() for codon in codons
}

### Sequences
compl_transtable = str.maketrans("aAcCgGtT", "tTgGcCaA")


def codons_from_ambiguous(codon: str) -> list:
    c1, c2, c3 = (
        (ambiguous_nt_to_nt[c] if c in ambiguous_nt_to_nt else c) for c in codon
    )
    return tuple("".join(c) for c in itertools.product(c1, c2, c3))


def aa_from_codon(codon: str) -> str:
    codons = codons_from_ambiguous(codon)
    matching_aa = set(
        (codontable_without_ambiguous[c] if c in codontable_without_ambiguous else "X")
        for c in codons
    )
    return matching_aa.pop() if len(matching_aa) == 1 else "X"


possible_nts = set("ATGC") | set(ambiguous_nt_to_nt.keys())
possible_codons = ("".join(c) for c in itertools.product(possible_nts, repeat=3))

codontable = {codon: aa_from_codon(codon) for codon in possible_codons}

def complement(seqn):
    return seqn.translate(compl_transtable)

def translate(seqn):
    """
    Returns all the aminoacid sequences from a one nucleic sequence
    nucleic -> (start,end),(prot,cropednucl)
    """
    codons = ((p * 3, seqn[p * 3: p * 3 + 3]) for p in range((len(seqn)) // 3))
    aminoacids = ((c[0], codontable[c[1]]) for c in codons)
    # itertools.groupby splits with stop codons
    # (stop codons are None and will cause bool to be False)
    seplist = (
        list(c) for b, c in itertools.groupby(aminoacids, lambda c: bool(c[1])) if b
    )
    # merge petptides
    return (((p[0][0] + 1, p[-1][0] + 3), "".join([a[1] for a in p])) for p in seplist)

def get_all_translations(seqn, minLength=None, maxLength=None):
    def real_coords_and_crop_nucl(seqptuple, frame, sense, seqn):
        (start, end), seqp = seqptuple
        return (
            (start + frame, end + frame), (seqp, seqn[start + frame - 1: end + frame]),
        )

    seqn = seqn.upper()
    revseqn = "".join(reversed(complement(seqn)))
    for (sense, seq) in (("+", seqn), ("-", revseqn)):
        for frame in range(3):
            seqp = tuple(
                filter(
                    lambda s: (minLength <= len(s[1]) if minLength else True) and (
                        len(s[1]) <= maxLength if maxLength else True),
                    translate(seq[frame:]),
                )
            )
            if seqp:
                yield (
                    (sense, frame + 1),
                    (real_coords_and_crop_nucl(s, frame, sense, seq)for s in seqp),
                )

def translate_nuc_to_prot_from_file(input_nuc_file, output_prot_file):
    with open(output_prot_file, "w") as output:
        for identifier_nuc, sequence in new_read_fasta_file(input_nuc_file):
            for ((sense, frame), peptides) in get_all_translations(sequence, 50):
                sense_str = "reversed_sense_" if sense == "-" else ""
                for ((start, end), (seqp, _)) in peptides:
                    identifier_pep = identifier_nuc + "_translated_sense_" + sense_str + str(start) + "_" + str(end) + \
                                     "_" + str(end)
                    write_sequence_in_open_file((identifier_pep, seqp), output)


def check_translation(file_aa, file_nuc):
    aa_sequence_dict = {identifier: sequence.replace('-', '') for identifier, sequence in new_read_fasta_file(file_aa)}
    both_sequences = ((identifier, aa_sequence_dict[identifier], sequence) for identifier, sequence in new_read_fasta_file(file_nuc))
    both_sequences_with_codon = ((identifier, seq_aa, [(seq_nuc[i:i+3]) for i in range(0, len(seq_nuc), 3)] ) for identifier, seq_aa, seq_nuc in both_sequences)
    ok = True
    for identifier, seq_aa, seq_nuc in both_sequences_with_codon:
        for idx, codon in enumerate(seq_nuc):
            if codon_to_aa[codon.upper()] != seq_aa[idx].upper():
                ok = False
                print('problem in sequence: ', identifier)
                print(idx, codon_to_aa[codon], seq_aa[idx], codon)
                break
    if ok:
        print("translation checked")
        return True
    else:
        return False

def check_translation_simple(nuc, aa):
    codons = (nuc[p * 3: p * 3 + 3] for p in range((len(nuc)) // 3))
    aminoacids = "".join([codontable[c] for c in codons if codontable[c]])
    return aminoacids == aa

def check_translation_one_sequence(identifier, prot_seq, nuc_seq, verbose = False):
    prot_seq_no_del = prot_seq.rstrip().replace('-', '').rstrip()
    nuc_seq = nuc_seq.rstrip()
    codon_seq = [(nuc_seq[i:i + 3]) for i in range(0, len(nuc_seq), 3)]
    if len(codon_seq) == len(prot_seq_no_del) + 1:
        codon_seq = codon_seq[:-1]
    for idx, codon in enumerate(codon_seq):
        if aa_from_codon(codon.upper()) != prot_seq_no_del[idx].upper():
            if verbose:
                print('problem in sequence: ', identifier)
                print(idx, aa_from_codon(codon.upper()), prot_seq_no_del[idx], codon)
            return False
    return True