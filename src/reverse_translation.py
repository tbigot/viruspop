import argparse
import itertools
import operator
import os
import random
import sys

sys.path.append(os.environ.get('current_dir'))
from utils_fasta import write_sequence_in_open_file, new_read_fasta_file
from translation import aa_to_nt_codon, codon_to_aa, check_translation


def build_dictionary_of_closest_coding(reader_prot_and_nuc_sequence_only):
    reader_prot_cds_aligned = ((itertools.accumulate(s_aa, lambda acc, aa:
    (acc[0] + 3, aa, s_nuc[acc[0]:acc[0] + 3]) if (
            aa != '-' and aa != '.' and s_nuc[acc[0]:acc[0] + 3] in codon_to_aa) else (acc[0] + 3, '-', '---') if (
            aa != '-' and aa != '.') else (acc[0], '-', '---'),
                                                     initial=(0, '', ''))) for _, s_aa, s_nuc in
                               reader_prot_and_nuc_sequence_only)
    aln_sequence_parallel_reader = zip(
        *([(AA, codon) for idx, AA, codon in reader if AA] for reader in reader_prot_cds_aligned))

    parallel_reader_grouped_by_aa = (itertools.groupby(sorted(aln, key=operator.itemgetter(0)), operator.itemgetter(0))
                                     for aln in
                                     aln_sequence_parallel_reader)
    parallel_reader_with_dictionary = ({aa: [coding[1] for coding in codings] for aa, codings in groups} for groups in
                                       parallel_reader_grouped_by_aa)

    parallel_reader_with_presence_tab_all_aa = (
        [[] if aa not in coding_at_idx else (0, coding_at_idx[aa]) for aa in aa_to_nt_codon if aa] for
        coding_at_idx in parallel_reader_with_dictionary)
    coding_along_sequence_for_each_aa = zip(*parallel_reader_with_presence_tab_all_aa)
    propagate_right = (
        itertools.accumulate(aa_tab, lambda acc, x: (acc[0] + 1, acc[1]) if acc and not x else x, initial=[]) for
        aa_tab
        in coding_along_sequence_for_each_aa)
    reverse = (reversed(list(one_codon_nearest)) for one_codon_nearest in propagate_right)

    propagate_left = [itertools.accumulate(aa_tab, lambda acc, x:
    (acc[0] + 1, acc[1]) if ((not x and acc) or (acc and x and acc[0] + 1 < x[0]))
    else (x[0], acc[1] + x[1]) if x and acc and acc[0] + 1 == x[0]
    else x, initial=None) for aa_tab in reverse]

    closest_neighbor_coding_for_each_aa = list(
        list(reversed(list((codons[1] if codons else []) for codons in one_codon_nearest))) for one_codon_nearest in
        propagate_left)

    dictionary_of_closest_coding = {aa: closest_neighbor_coding_for_each_aa[idx] for idx, aa in
                                    enumerate((aa for aa in aa_to_nt_codon if aa))}

    return dictionary_of_closest_coding


def get_original_sequences(all_protein_file, nuc_ref_file):
    ref_aa_seq_generator = (
        (seq[0], seq[1]) for seq in
        new_read_fasta_file(all_protein_file) if 'Node' not in seq[0])
    nuc_dict = {identifier: sequence for identifier, sequence in new_read_fasta_file(nuc_ref_file)}
    reader_prot_and_nuc_sequences = (
        (identifier, prot_sequence, nuc_dict[identifier]) for
        identifier, prot_sequence in ref_aa_seq_generator if identifier in nuc_dict)
    return reader_prot_and_nuc_sequences


def reverse_translate(gen_prot, translation_references):
    nuc_seq = ''
    for idx, aa in enumerate(gen_prot):
        if aa in translation_references:
            if translation_references[aa][idx]:
                nuc_seq += random.choice(translation_references[aa][idx])
            else:
                nuc_seq += random.choice(aa_to_nt_codon[aa])
        else:
            if aa != '-' and aa != '.':
                print(aa)
                nuc_seq += '---'
    return nuc_seq


def reverse_translation(all_protein_file, nuc_ref_file, output_file):
    reader_prot_and_nuc_sequence = get_original_sequences(all_protein_file, nuc_ref_file)
    closest_coding = build_dictionary_of_closest_coding(reader_prot_and_nuc_sequence)
    with open(output_file, 'w') as output:
        for identifier, seq in new_read_fasta_file(all_protein_file):
            if 'Node' in identifier:
                write_sequence_in_open_file((identifier, reverse_translate(seq, closest_coding) + '\n'),
                                            output)
    if not check_translation(all_protein_file, output_file):
        os.remove(output_file)
        raise Exception("Problem in retro-translation")


## \cond
parser = argparse.ArgumentParser()
parser.add_argument("all_protein_file", help="Input fasta file")
parser.add_argument("nuc_ref_file", help="Output file")
parser.add_argument("output_file_name", help="Output file")
parser.add_argument("-s", "--seed", help="Random seed")
args = parser.parse_args()
## \endcond

if __name__ == "__main__":
    if args.seed:
        random.seed(args.seed)
    reverse_translation(args.all_protein_file, args.nuc_ref_file, args.output_file_name)
