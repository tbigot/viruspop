#!/usr/bin/env python3
"""! Groups leaves in a phylogenetic tree with their neighbor.
Each leaf is grouped with a neighbouring leaf if their distance is below a threshold.
@section libraries_main Libraries/Modules
<a href="https://dendropy.org/primer/index.html">Dendropy</a> """

import dendropy
import argparse



def cluster_tree_leaves(tree_file, dist_threshold):
    """!
    @param tree_file: Newick formatted tree file
    @param output_file: Directory and prefix for output
    @param dist_threshold: distance threshold
    """

    def output_with_prefix(cluster_dic, output_file, tree):
        for taxon in tree.taxon_namespace:
            taxon.label = taxon.label + "_cluster" + ('0' if cluster_dic[taxon] < 10 else "") + str(cluster_dic[taxon])
        tree.write(path=output_file, schema="newick")

    def extract_mrca(tree, cluster_list, distance_matrix):
        dict_nodes = {}
        for cluster in cluster_list:
            if cluster and len(cluster) > 1:
                mrca = tree.mrca(taxa=cluster)
                filter = lambda x : x in cluster
                m = distance_matrix.mean_pairwise_distance(filter_fn=filter)
                dict_nodes[mrca.label] = m
            # elif cluster: TODO isolated nodes are left behind. Find another solution ?
            #     list_nodes.append(cluster[0].label)

        return dict_nodes


    tree = dendropy.Tree.get(path=tree_file, schema='newick')
    distance_matrix = tree.phylogenetic_distance_matrix()
    cluster_dic = {}
    cluster_list = []
    i = 0
    for idx1, taxon1 in enumerate(tree.taxon_namespace):
        if taxon1 not in cluster_dic:
            cluster_dic[taxon1] = i
            cluster_list.append([taxon1])
            i += 1
        for taxon2 in tree.taxon_namespace:
            if taxon1 is not taxon2:
                is_nearby = distance_matrix.patristic_distance(taxon1, taxon2) <= dist_threshold
                if is_nearby:
                    if taxon2 not in cluster_dic:
                        cluster_dic[taxon2] = cluster_dic[taxon1]
                        cluster_list[cluster_dic[taxon1]].append(taxon2)
                    elif cluster_dic[taxon1] != cluster_dic[taxon2]:
                        idx_to_remove = cluster_dic[taxon1]
                        for taxon_to_move in cluster_list[idx_to_remove]:
                            cluster_dic[taxon_to_move] = cluster_dic[taxon2]
                            cluster_list[cluster_dic[taxon2]].append(taxon_to_move)
                        cluster_list[idx_to_remove] = []
    output_with_prefix(cluster_dic, tree_file + ".clusters", tree)
    list_nodes = extract_mrca(tree, cluster_list, distance_matrix)
    return list_nodes


if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description="Not adapted for shell usage")
    parser.add_argument("tree_file", help="Newick formatted tree file")
    parser.add_argument("dist_threshold", help="Maximum distance between sequences for clustering")
    args = parser.parse_args()
    ## \endcond
    cluster_tree_leaves(args.tree_file, args.dist_threshold)
