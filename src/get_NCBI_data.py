"""!
Script for fetching a list of taxon from the NCBI nucleotide database.


The input file is a list of taxon references.

directory/to/myExample.txt :
~~~~~~~~~~~~~~~~~~~~~.txt
GENBANK|AY729016
GENBANK|AF092942
KT992094
AF013254
~~~~~~~~~~~~~~~~~~~~~

The outputs are :

For each taxon:
<ul>
<li>A fasta formatted file containing the full nucleotide sequence

<strong>outputDirectory/AY729016.nuc</strong>:
~~~~~~~~~~~~~~~~~~~~~.txt
>AF013254|Human respiratory syncytial virus wildtype strain B1, complete genome
acgcgaaaaaatgcgtactacaaacttgcacattcggaaaaaatggggcaaataagaatttg...
~~~~~~~~~~~~~~~~~~~~~
</li>

<li>A fasta formatted file containing all CDS

<strong>outputDirectory/AY729016.aa</strong>:
~~~~~~~~~~~~~~~~~~~~~.txt
>AF013254|AAB82429.1|99..518|non-structural protein 1
MGCNSLSMIKVRLQNLFDNDEVALLKITCYTDKLILLTNALAKAAIHTIKLNGIVFIHVITSSEVCPDNNIVVKSNFTTMPILQNGGYIWELIELTHCSQLNGLMDDNCEIKFSKRLSDSVMTNYMNQISDLLGLDLNS
>AF013254|AAB82430.1|626..1000|non-structural protein 2
MSTTNDNTTMQRLMITDMRPLSMDSIITSLTKEIITHKFIYLINNECIVRKLDERQATFTFLVNYEMKLLHKVGSTKYKKYTEYNTKYGTFPMPIFINHGGFLECIGIKPTKHTPIIYKYDLNP
...
~~~~~~~~~~~~~~~~~~~~~
</li>
</ul>

A file combining all `*.aa` files

<strong>outputDirectory/pro.fasta</strong>

@section libraries_main Libraries/Modules
<a href="https://biopython.org/docs/1.76/api/Bio.Entrez.html">Bio.Entrez</a>

"""
import argparse
import os

from Bio import Entrez

def getNCBIdata(filename, output_directory):
    """!
    @param filename: File containing a list of taxon (one per line).
    @param output_directory: Directory for output files
    """

    # Creating directory if does not exist
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    # Opening the file and reading the list of taxa
    f = open(filename)
    Entrez.email = "eigtw510tyjrt403@gmail.com"
    virusRawGen = (line.split('|')[-1] for line in (line_raw.strip() for line_raw in f) if (line and not line.startswith("#")))

    # Fetching the data from NCBI website
    ids = [id for id in virusRawGen]
    # virusFetcherGen = (Entrez.efetch(db="nucleotide", id=virus, retmode="xml") for virus in virusRawGen)
    # virusDicGen = (list(Entrez.read(data))[0] for data in virusFetcherGen)
    fetched_data = Entrez.read(Entrez.efetch(db="nucleotide", id=ids, retmode="xml"))

    # function for looking for a data with label keyStr in the not very convenient virusDic structure
    def getDataInQual(virusDic, keyStr):
        for dict in virusDic["GBFeature_quals"]:
            try:
                if dict["GBQualifier_name"] == keyStr:
                    return dict["GBQualifier_value"]
            except:
                print("do nothing")
        return ''

    # For each taxon
    for virusDict in fetched_data:
        taxon = virusDict["GBSeq_locus"]
        # Retrieves and write the whole nucleotide sequence
        with open(output_directory + taxon + ".nuc", "w") as o:
            o.write(f">{taxon}|{virusDict['GBSeq_definition']}\n")
            o.write(f"{virusDict['GBSeq_sequence']}\n")
        # Retrieves and write each protein sequence
        with open(output_directory + taxon + ".aa", "w") as o:
            for feature in virusDict['GBSeq_feature-table']:
                if isinstance(feature, dict):
                    if "GBFeature_key" in feature and feature["GBFeature_key"] == "CDS":
                        o.write(
                            f">{taxon}|{getDataInQual(feature, 'protein_id')}|{feature['GBFeature_location']}|{getDataInQual(feature, 'product')}\n")
                        o.write(f"{getDataInQual(feature, 'translation')}\n")
    f.close()

if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description="Script for fetching a list of taxon from the NCBI nucleotide "
                                                 "database.")
    parser.add_argument("filename", help="File containing a list of taxon (one per line).")
    parser.add_argument("output_directory", help="Directory for output files")
    args = parser.parse_args()
    ## \endcond
    getNCBIdata(args.filename, args.output_directory)

