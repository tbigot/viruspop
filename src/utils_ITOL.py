import dendropy
from pathlib import Path
from itolapi import Itol

NUM2COLOURS = {
    1: ['#1b9e77'],
    2: ['#d95f02', '#1b9e77'],
    3: ['#a6cee3', '#1f78b4', '#b2df8a'],
    4: ['#66c2a5', '#fc8d62', '#8da0cb', '#e78ac3'],
    5: ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00'],
    6: ['#7fc97f', '#beaed4', '#fdc086', '#ffff99', '#386cb0', '#f0027f']
}

WHITE = '#ffffff'

def upload_to_itol(tree_path, dataset_paths=[], tree_name=None, tree_description=None, project_name=None, upload_id=None, success_message=None):
    try:
        itol_uploader = Itol()
        itol_uploader.add_file(Path(tree_path))
        for annotation_file in dataset_paths:
            itol_uploader.add_file(Path(annotation_file))
        if tree_name:
            itol_uploader.params['treeName'] = tree_name
        if tree_description:
            itol_uploader.params['treeDescription'] = tree_description
        if upload_id:
            itol_uploader.params['uploadID'] = upload_id
            if project_name:
                itol_uploader.params['projectName'] = project_name
        if itol_uploader.upload():
            if success_message:
                print(success_message)
                print(
                    'Tree ({}) uploaded to iTOL: {}.'.format(itol_uploader.comm.tree_id,
                                                                               itol_uploader.get_webpage()))
            else:
                print(
                    'Successfully uploaded your tree ({}) to iTOL: {}.'.format(itol_uploader.comm.tree_id,
                                                                           itol_uploader.get_webpage()))
            return itol_uploader.comm.tree_id, itol_uploader.get_webpage()
        else:
            status = itol_uploader.comm.upload_output
    except Exception as e:
        status = e
    print(
        'Failed to upload your tree to iTOL because of "{}". Please check your internet connection and iTOL settings{}.'
            .format(status,
                    (', e.g. your iTOL batch upload id ({}){}'
                     .format(upload_id,
                             (' and whether the project {} exists'.format(project_name) if project_name else '')))
                    if upload_id else ''))
    return None, None


STYLE_FILE_HEADER_TEMPLATE = """DATASET_STYLE

SEPARATOR TAB
DATASET_LABEL	{dataset_label}
COLOR	#ffffff

LEGEND_COLORS	{colours}
LEGEND_LABELS	{labels}
LEGEND_SHAPES	{shapes}
LEGEND_TITLE	{dataset_label}

DATA
#NODE_ID TYPE   NODE  COLOR SIZE_FACTOR LABEL_OR_STYLE
"""

# Iterate over all leaves in one node sub-tree
def child_leaf_iter(node: dendropy.Node):
    for nd in node.child_node_iter():
        yield from child_leaf_iter(nd)
    if node.taxon is not None:
        yield node

# root from midpoint for ITOL visualization
def reroot_tree_for_visualisation(tree_file, rooted_tree_file):
    tree = dendropy.Tree.get_from_path(tree_file, "newick")
    tree.reroot_at_midpoint()
    tree.write_to_path(rooted_tree_file, 'newick')

# creates an ITOL style file with colored branches for the simulations
# TODO issue with the branches to a single simulation
def create_style_file_with_simulation(tree_file, prefix):

    tree = dendropy.Tree.get_from_path(tree_file, 'newick')
    leaf_gen = ((leaf, leaf.taxon.label) for leaf in tree.leaf_node_iter() if leaf.taxon is not None)
    for leaf, label in leaf_gen:
        new_label = label.replace(" ", "_").replace(",", "_").replace('(', '_').replace(')', '_').replace(':',
                                                                                                          '_').replace(
            "__", "_")
        leaf.taxon.label = new_label

    leaf_label = [node.taxon.label for node in tree.leaf_node_iter()]
    simulations = [label for label in leaf_label if "Node" in label]
    reals = [label for label in leaf_label if "Node" not in leaf_label]

    node_to_color = [node for node in tree.postorder_node_iter() if all("Node" in sub_node.taxon.label for sub_node in child_leaf_iter(node)) and not node.taxon]
    for k, node in enumerate(node_to_color):
        node.label = "n" + str(k)


    dataset_label = "VIRUS POP"
    colours = ['#000000', '#1b9e77']
    labels = ["Real", "Simulated"]

    style_simulations = "\t".join(["label", "node", colours[1], "1", "bold", WHITE])
    style_reals = "\t".join(["label", "node", colours[0], "1", "normal", WHITE])
    style_branch_simu = "\t".join(["branch", "clade", colours[1], "1", "bold", WHITE])

    style_file = prefix + '.style.txt'
    with open(style_file, 'w') as sf:
        sf.write(STYLE_FILE_HEADER_TEMPLATE
                 .format(dataset_label=dataset_label, colours='\t'.join(colours), labels='\t'.join(labels),
                         shapes='\t'.join(['1'] * len(labels))))

        for node_name in reals:
            if '-' in node_name or '_' in node_name or "[" in node_name or " " in node_name:
                sf.write("'" + node_name + "'" + "\t" + style_reals + "\n")
            else:
                sf.write(node_name + "\t" + style_reals + "\n")
        for node_name in simulations:
            if '-' in node_name or '_' in node_name or "[" in node_name or " " in node_name:
                sf.write("'" + node_name + "'" + "\t" + style_simulations + "\n")
            else:
                sf.write(node_name + "\t" + style_simulations + "\n")
        for node in node_to_color:
            sf.write(node.label + "\t" + style_branch_simu + "\n")

    tree.reroot_at_midpoint()
    tree.write_to_path(prefix + ".tree", 'newick')

    return prefix + ".tree", style_file



