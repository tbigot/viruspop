#!/usr/bin/env python3
import argparse
import inspect
import itertools
import operator
import os
import random
import sys
import dendropy

sys.path.append(os.environ.get('current_dir'))
import utils_fasta
from cluster_tree_leaves import cluster_tree_leaves


def extract_nodes_data(state_file_from_iqtree, node_list, partition_file):
    # Creats a list of (site, partition_number, site_in_partition_number)
    partition_line = (l.replace(';', '').strip().split()[3:] for l in open(partition_file) if
                      l.split()[0] == 'charset')
    index_blocks_per_partition = [(block.split('-') for block in blocks if block != 'AA,') for blocks in partition_line]
    list_index_per_partition = (
        itertools.chain.from_iterable([i for i in range(int(block[0]), int(block[1]) + 1)] for block in blocks) for
        blocks
        in index_blocks_per_partition)
    site_part_site_in_part = [(loc, idx_partition + 1, idx + 1) for idx_partition, list_part in
                              enumerate(list_index_per_partition) for idx, loc in enumerate(list_part)]
    site_part_site_in_part_sorted = sorted(site_part_site_in_part, key=operator.itemgetter(0))

    # Creating {Node : {part: list_of_Node_Part_data}}
    node_groups = itertools.groupby(
        (sp for l in open(state_file_from_iqtree) if (sp := l.strip().split())[0] in node_list),
        key=operator.itemgetter(0))
    node_dic_of_part_groups = {node: itertools.groupby([sp for sp in sp_lines], operator.itemgetter(1)) for
                               node, sp_lines in node_groups}
    node_dic_of_part_dic = {node: {int(part): list(sp_lines) for part, sp_lines in grouped_parts} for
                            node, grouped_parts in node_dic_of_part_groups.items()}
    return site_part_site_in_part_sorted, node_dic_of_part_dic


def get_node_data(site_part_site_in_part_sorted, node_dic_of_part_dic, node, site: dict):
    part = site_part_site_in_part_sorted[site - 1][1]
    idx_in_part = site_part_site_in_part_sorted[site - 1][2] - 1
    return node_dic_of_part_dic[node][part][idx_in_part]


def get_sub_tree_data(treefile, node_list):
    def child_leaf_iter(node: dendropy.Node):
        for nd in node.child_node_iter():
            yield from child_leaf_iter(nd)
        if node.taxon is not None:
            yield node

    dict_nodes = {}
    tree = dendropy.Tree.get_from_path(treefile, 'newick')
    tree.reroot_at_midpoint()
    distance_matrix = tree.phylogenetic_distance_matrix()
    for node in node_list:
        list_sub_leaf_taxon = [node.taxon for node in child_leaf_iter(tree.find_node_with_label(node))]
        filter_leaf = lambda x: x in list_sub_leaf_taxon
        m = distance_matrix.mean_pairwise_distance(filter_fn=filter_leaf)
        dict_nodes[node] = m
    return dict_nodes


def build_nodes(state_file_from_iqtree, partition_file_from_iqtree, node_dict, consensus=True, n=1):
    data_partition, data_state = extract_nodes_data(state_file_from_iqtree, node_dict, partition_file_from_iqtree)
    length = len(data_partition)
    if consensus:
        aa_getter = lambda node_name, site: get_node_data(data_partition, data_state, node_name, site)[3]
    else:
        node_proba_getter = lambda node_name, site: list(
            map(float, get_node_data(data_partition, data_state, node_name, site)[4:]))
        list_AA = next([AA[2:3] for AA in sp if AA[0:2] == 'p_'] for l in open(state_file_from_iqtree) if
                       (sp := l.strip().split())[0] == "Node")
        aa_getter = lambda node_name, site: random.choices(list_AA, node_proba_getter(node_name, site))[0]
    return {node + "_" + str(i + 1) + "_" + str(node_dict[node]): ''.join(
        [aa_getter(node, idx + 1) for idx in range(length)]) for node in node_dict for i in range(n)}


def get_all_internal_node(treefile):
    tree = dendropy.Tree.get_from_path(treefile, 'newick')
    nodes = [node.label for node in tree.postorder_node_iter() if node.child_nodes()]
    return nodes


def build_common_ancestor_node_sequences(state_file_from_iqtree, partition_file_from_iqtree, treefile, output,
                                         node_file_list=False, cluster_max_dist=False, consensus=True, n=1):
    if node_file_list == "all":
        node_list = get_all_internal_node(treefile)
        node_dict = get_sub_tree_data(treefile, node_list)
    elif node_file_list:
        node_list = [l.strip() for l in open(node_file_list) if (l.strip() and not l.startswith(("#")))]
        node_dict = get_sub_tree_data(treefile, node_list)
    elif cluster_max_dist:
        node_dict = cluster_tree_leaves(treefile, cluster_max_dist)
    else:
        node_list = ["Node" + str(i + 1) for i in range(open(treefile).read().count("Node"))]
        node_dict = get_sub_tree_data(treefile, node_list)
    asr_dic = build_nodes(state_file_from_iqtree, partition_file_from_iqtree, node_dict, consensus, n)
    with open(output, 'w') as output:
        for node, sequence in asr_dic.items():
            utils_fasta.write_sequence_in_open_file((node, sequence), output)


if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(
        description="If both node_file and cluster_max_dist are ignored, all ancestor node sequences will be built.")
    parser.add_argument("state_file_from_iqtree", help="State file from iqtree")
    parser.add_argument("partition_file", help="Partition file (nexus format)")
    parser.add_argument("treefile", help="treefile")
    parser.add_argument("output_file", help="Output file (fasta format)")
    parser.add_argument("-f", "--node_file", help="File with a list of nodes where the simulation will be started")
    parser.add_argument("-d", "--cluster_max_dist", help="Max distance between two nodes in a cluster")
    parser.add_argument("-r", "--no_consensus", action='store_true',
                        help="create random ancestral sequences from state posterior probability. Default (without this flag) will build the consensus sequence at each node.")
    parser.add_argument("-n", "--number",
                        help="Number of sequences to generate for each node (default = 1) (if consensus sequences are built, it will return n times the same sequence")
    args = parser.parse_args()
    ## \endcond
    consensus = True if not args.no_consensus else False
    n = 1 if not args.number else int(args.number)
    if args.cluster_max_dist:
        build_common_ancestor_node_sequences(args.state_file_from_iqtree, args.partition_file, args.treefile,
                                             args.output_file,
                                             cluster_max_dist=float(args.cluster_max_dist), consensus=consensus, n=n)
    elif args.node_file:
        build_common_ancestor_node_sequences(args.state_file_from_iqtree, args.partition_file, args.treefile,
                                             args.output_file,
                                             node_file_list=args.node_file, consensus=consensus, n=n)
    else:
        build_common_ancestor_node_sequences(args.state_file_from_iqtree, args.partition_file, args.treefile,
                                             args.output_file,
                                             consensus=consensus, n=n)
