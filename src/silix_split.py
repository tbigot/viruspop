#!/usr/bin/env python3
"""!
Replaces silix-split that doesn't seem to work and generates a protein cluster name.
From a clustering from SiLiX and a fasta file containing all sequences, builds a fasta file for each cluster with
min_size < size < max_size. Constructs file names based on cluster sequence identifiers.
"""
import argparse
import sys, os
import itertools
import operator

sys.path.append(os.environ.get('current_dir'))
from utils_fasta import new_read_fasta_file, write_sequence_in_open_file


def silix_split(fasta_file, silix_file, output_directory, min_size, max_size = None):
    """!
    @param fasta_file: Input fasta file
    @param silix_file: Fnodes file output produced by silix
    @param output_directory: Output directory
    @param min_size: Minimum size of clusters
    @param max_size: Maximum size of clusters
    """

    def get_and_filter_clusters(silix_file):
        # Reads silix cluster file, groups the identifiers by cluster and filters with max and min size
        cluster_grouper = itertools.groupby(sorted(((ls := line.split())[0], ls[1])
                                                   for line in open(silix_file)), key=operator.itemgetter(0))


        # cluster_grouper_filtered = ([item[1] for item in cluster_list]
        #                             for cluster in cluster_grouper if
        #                             max_size >= len(cluster_list := list(cluster[1])) >= min_size)

        cluster_grouper_filtered = ([item[1] for item in cluster_list]
                                    for cluster in cluster_grouper if
                                    len(cluster_list := list(cluster[1])) >= min_size)


        #In case some clusters contain two sequences from the same taxon, the whole cluster is removed
        #Bad idea. For now, let's just leave all the clusters.
        # TODO: something else ?
        # cluster_gen_and_list_of_taxon = ((cluster, [l.split('|')[0] for l in cluster]) for cluster in cluster_grouper_filtered)
        # cluster_grouper_no_repetition = (gen1 for gen1, list_tax in cluster_gen_and_list_of_taxon if len(list_tax) == len(set(list_tax)))

        return cluster_grouper_filtered

    clusters_generator = get_and_filter_clusters(silix_file)

    # Reads fasta file and create a dictionary of all sequences
    sequence_dictionary = {identifier: (identifier, seq) for identifier, seq in new_read_fasta_file(fasta_file)}

    with open(os.path.join(output_directory, 'homologous_protein_list.txt'), 'w') as protein_list_output:
        for idx, cluster in enumerate(clusters_generator):
            N = len(cluster)
            count = {}
            words = ((word, idx_word) for idt in cluster for idx_word, word in
                     enumerate(idt.split('|')[-1].replace('-', '_').split('_')))
            for word, idx_word in words:
                count[word] = [count[word][0] + 1, (count[word][1] * count[word][0] + idx_word) / (
                        count[word][0] + 1)] if word in count else [1, idx_word]

            prot_name = '_'.join(word if word_info[0] > int(N / 2) else '[' + word + ']' for word, word_info in
                                 sorted(count.items(), key=lambda item: item[1][1]) if word_info[0] > int(N / 3))
            prot_name = prot_name.replace("/", "_").replace("\\", "_")
            if prot_name == "":
                prot_name = "no_name"
            file_name = 'cl' + str(idx + 1) + '_' + str(N) + '_' + prot_name
            protein_list_output.write(file_name + '\n')

            with open(os.path.join(output_directory, file_name + '.fa'), 'w') as output:
                for identifier in cluster:
                    write_sequence_in_open_file(sequence_dictionary[identifier], output)


## \cond
parser = argparse.ArgumentParser(description = "Replaces silix-split that doesn't seem to work and generates a protein cluster name.")
parser.add_argument("fasta_file", help="Input fasta file")
parser.add_argument("silix_file", help="Input silix clustering file")
parser.add_argument("output_directory", help="Output directory")
parser.add_argument("min_size", help="Minimum size of clusters")
args = parser.parse_args()
## \endcond

if __name__ == "__main__":
    silix_split(args.fasta_file, args.silix_file, args.output_directory, int(args.min_size))
