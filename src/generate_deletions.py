#!/usr/bin/env python3

import os, sys
import dendropy
import argparse
from random import choice, random

sys.path.append(os.environ.get('current_dir'))
from utils_fasta import new_read_fasta_file, write_sequence_in_open_file

def child_leaf_iter(node: dendropy.Node):
    for nd in node.child_node_iter():
        yield from child_leaf_iter(nd)
    if node.taxon is not None:
        yield node

def add_deletion(sim_seq, list_sequence):
    i = 0
    new_seq = []
    while i < len(sim_seq):
        seq = choice(list_sequence)
        if seq[i] == '-' and (i == 0 or seq[i-1] != '-') :
            while i < len(sim_seq) and seq[i] == '-':
                new_seq.append('-')
                i+=1
        else:
            new_seq.append('-' if all(l[i] == '-' for l in list_sequence) else sim_seq[i])
            i += 1
    return ''.join(new_seq)

def convert_name_for_tree(name):
    return name.strip().replace('_', ' ').replace('(', ' ').replace(')', ' ').replace(',', ' ').replace('<', ' ')\
        .replace('>', ' ').replace('^', ' ').replace("'", " ").replace("?", " ").replace(":", " ").replace(";", " ").replace(";", " ").replace("+", " ")


# Simulates leaves must be named following 'Node12_...' With the node number at which the ancestral sequence was generated to initiate the simulation
def generate_deletion_based_on_sub_tree(tree_file_with_node, all_sequence_aln_file, output_file, seed = 0):
    tree = dendropy.Tree.get_from_path(tree_file_with_node, 'newick')
    dict_node_name_to_node = {}
    dict_nodes_to_sim = {}
    dict_initial_seq = {}
    for identifier, sequence in new_read_fasta_file(all_sequence_aln_file):
        if 'Node' in identifier:
            if identifier.split("_")[0] not in dict_nodes_to_sim:
                try:
                    node = tree.find_node_with_label(identifier.split("_")[0])
                except:
                    print("No node found with label '" + identifier.split("_")[0] + "'. You should check the tree file.")
                    return
                dict_node_name_to_node[identifier.split("_")[0]] = node
                dict_nodes_to_sim[identifier.split("_")[0]] = []
            dict_nodes_to_sim[identifier.split("_")[0]].append((identifier, sequence))
        else:
            seq_id = convert_name_for_tree(identifier)
            dict_initial_seq[seq_id] = sequence


    if not dict_node_name_to_node:
        print("Error : No simulated sequence found")
        return

    with open(output_file, 'w') as output:
        not_simulated = ( (identifier, sequence) for identifier, sequence in new_read_fasta_file(all_sequence_aln_file) if ('Node' not in identifier))
        for fasta_seq in not_simulated:
            write_sequence_in_open_file(fasta_seq, output)
        for node_name, node in dict_node_name_to_node.items():
            sub_leaves = child_leaf_iter(node)
            list_sequence = [dict_initial_seq[str(node.taxon)[1:-1]] for node in sub_leaves]
            for sim_id, sim_seq in dict_nodes_to_sim[node_name]:
                new_sim_seq = add_deletion(sim_seq, list_sequence)
                write_sequence_in_open_file((sim_id, new_sim_seq), output)




if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser()
    parser.add_argument("tree_file_with_node", help="Newick tree file with initial sequences (no simulation) and node names.")
    parser.add_argument("all_sequence_aln_file", help="Fasta file with initial and generated sequences.")
    parser.add_argument("output_file", help="Fasta formated output file")
    parser.add_argument("-s", "--seed", help="Random seed")
    args = parser.parse_args()
    ## \endcond
    if args.seed:# for deletion
        random.seed(args.seed)
    generate_deletion_based_on_sub_tree(args.tree_file_with_node, args.all_sequence_aln_file, args.output_file)
