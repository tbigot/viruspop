#!/usr/bin/env python3
"""!
Small utility functions for manipulating fasta files.
"""
import itertools
import os
import sys
import warnings

sys.path.append(os.environ.get('current_dir'))


def crop_sequence(sequence_from_fasta, start, end):
    """!
    Crops a sequence with the given indexes.
    @param sequence_from_fasta: A tuple (identifier, sequence)
    @param start: Start index for cropping
    @param end: End index for cropping
    @return A tuple (identifier, sequence)
    """
    try:
        return sequence_from_fasta[0], sequence_from_fasta[1][start:end] + '\n'
    except IndexError as e:
        warnings.warn("Index out.save of range. Initial sequence is returned")
        return sequence_from_fasta


def new_read_fasta_file(file):
    """!
    Reads a fasta file (NO EXCEPTION HANDLING).
    @param file: Input fasta file
    @return A tuple (identifier, sequence) Generator
    """
    # TODO enlever le \n et > et mettre à jour les utilisations
    reader = itertools.groupby(open(file), lambda line: line.startswith(">"))
    groupProtein = itertools.accumulate(reader, lambda acc, x: (False, [next(x[1])[1:].strip()]) if x[0] else (
        True, acc[1] + [''.join(list(x[1])).replace("\n", "")]), initial=(False, None))
    sequenceGenerator = (p[1] for p in groupProtein if p[0])
    return sequenceGenerator


def write_sequence_in_open_file(sequence, open_file):
    """!
    Write a tuple (identifier, sequence) in an opened for writing file.
    @param sequence: A tuple (identifier, sequence)
    @param open_file: File object opened for writing.
    """
    if '>' != sequence[0][0]:
        open_file.write(f"{'>'}")
    for s in sequence:
        if '\n' not in s[-1]:
            s += '\n'
        open_file.write(f"{s}")










