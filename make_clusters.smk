import inspect
import os
import sys
import warnings

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, current_dir)
sys.path.insert(0, os.path.join(current_dir, "src"))

from src.translation import translate_nuc_to_prot_from_file
from src.utils_fasta import new_read_fasta_file, write_sequence_in_open_file

singularity: "singularity/virus_pop.sif"

#-------------------------------------------------------
# read config files

RULES = os.path.join("rules_make_clusters")

#-------------------------------------------------------
seed = config['seed']
#-------------------------------------------------------
dataset_type = config["data_type"]
dataset_info = config["data_info"]



scripts_dir = config["scripts_dir"]
nb_init_target = config['nb_init_target']
generated_data_dir = config['generated_data_dir']
if not generated_data_dir.endswith(os.path.sep):
    generated_data_dir += os.path.sep
fetched_data_dir = os.path.join(generated_data_dir, config['fetched_data_folder'])
cluster_dir = os.path.join(generated_data_dir, config['cluster_folder'])
#iqtree
seed = config['seed']
retro_translation_switch = True


seed_option = ""

if seed:
    seed_option = "--seed " + str(seed)




if dataset_type == "genome_directory":
    if os.path.isdir(dataset_info):
        genome_directory = dataset_info
        nuc_files = [os.path.join(genome_directory, file) for file in os.listdir(genome_directory) if file.endswith(".nuc")]
        aa_files = [os.path.join(genome_directory, file) for file in os.listdir(genome_directory) if file.endswith(".aa")]
        no_aa_file = ([file for file in nuc_files if not file.replace(".nuc",".aa")])
        no_nuc_file = ([file for file in aa_files if not file.replace(".aa", ".nuc")])
        if not nuc_files and not aa_files:
            print("There is no file with '.aa' nor '.nuc' extension in the input folder.")
            exit(0)
        if no_nuc_file:
            warnings.warn("There is no complete genome for each protein file. The retro-translation is deactivated")
            retro_translation_switch = False
        with open(os.path.join(genome_directory, config["protein_list_file"]), 'w') as output_protein_list:
            for file in no_aa_file:
                translate_nuc_to_prot_from_file(file, file.replace(".nuc", ".aa"))
                for identifier, sequence in new_read_fasta_file(file.replace(".nuc", ".aa")):
                    write_sequence_in_open_file((identifier, sequence), output_protein_list)
            for file in aa_files:
                for identifier, sequence in new_read_fasta_file(file):
                    write_sequence_in_open_file((identifier, sequence), output_protein_list)
        make_cluster_input_protein_list = genome_directory + config["protein_list_file"]
    else:
        print("Input directory does not exist")
        exit(0)
else:
    if dataset_type == "taxon_file":
        if os.path.isfile(dataset_info):
            taxon_file = dataset_info
        else:
            print("Protein file not found.")
            exit(0)
    else:
        if dataset_type == "group_name":
            create_tax_list_from_clade_input = dataset_info
            taxon_file = generated_data_dir + config["list_taxon_file"]
            create_tax_list_from_clade_output = taxon_file
            include: os.path.join(RULES,"1-create_tax_list_from_clade.rules")

    fetch_input_list_taxon_file = taxon_file
    fetch_output_list_protein_file = fetched_data_dir + "prot.fasta"
    include: os.path.join(RULES,"2-fetch.rules")
    make_cluster_input_protein_list = fetched_data_dir + "prot.fasta"

make_cluster_output_cluster_list_file = cluster_dir + config["homologous_protein_list_file"]
include: os.path.join(RULES,"3-make_clusters.rules")



rule all:
    input:
        make_cluster_output_cluster_list_file