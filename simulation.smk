import inspect
import os
import shutil
import sys

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(current_dir, "src"))



singularity: "singularity/virus_pop.sif"

#-------------------------------------------------------
# read config files

RULES = os.path.join("rules_simulation")

#-------------------------------------------------------
seed = config['seed']
#-------------------------------------------------------
dataset_type = config["data_type"]
dataset_info = config["data_info"]



scripts_dir = config["scripts_dir"]
nb_init_target = config['nb_init_target']
generated_data_dir = config['generated_data_dir']
if not generated_data_dir.endswith(os.path.sep):
    generated_data_dir += os.path.sep
fetched_data_dir = os.path.join(generated_data_dir, config['fetched_data_folder'])
cluster_dir = os.path.join(generated_data_dir, config['cluster_folder'])
iqtree_dir = os.path.join(generated_data_dir, config['iqtree_folder'])
simulation_dir = os.path.join(generated_data_dir, config['simulation_folder'])
ancestral_sequence_dir = os.path.join(generated_data_dir, config['ancestral_sequence_folder'])
min_size_cluster = config['min_size_cluster']
#iqtree
seed = config['seed']
retro_translation_switch = True


seed_option = ""

if seed:
    seed_option = "--seed " + str(seed)


if dataset_type == "homologous_protein_directory":
    if not  os.path.isdir(dataset_info):
        print("Input directory does not exist")
        exit(0)
    list_homologous =[dataset_info for file in os.listdir(dataset_info)]
    directory_input_find_substitution = dataset_info if dataset_info.endswith("/") else dataset_info + "/"
elif dataset_type == "homologous_protein_file":
    if not  os.path.isfile(os.path.abspath(dataset_info)):
        print("Input file does not exist : " + os.path.abspath(dataset_info))
        exit(0)
    list_homologous = [os.path.split(dataset_info)[1]]

    if not os.path.isdir(cluster_dir):
        os.makedirs(cluster_dir)
    if not os.path.isfile(os.path.join(cluster_dir, os.path.basename(dataset_info))):
        shutil.copyfile(dataset_info,os.path.join(cluster_dir, os.path.basename(dataset_info)))
    directory_input_find_substitution = cluster_dir
else:
    directory_input_find_substitution = cluster_dir
    list_homologous = [file for file in os.listdir(cluster_dir) if file.startswith("cl") and int(file.split("_")[1]) >= min_size_cluster and not file.endswith('.nuc')]

simulation_output_all_files = [simulation_dir + file + ".all.aln" for file in list_homologous]
if dataset_type != "homologous_protein_file":
    simulation_output_all_files += [simulation_dir + file + ".simu.nuc" for file in list_homologous]

if config['generate_final_tree'] == "yes" or (type(config['generate_final_tree']) == bool and config['generate_final_tree']):
    simulation_output_all_files += [os.path.join(iqtree_dir, "with_simulation", file + ".aln.nex.best_model.nex.treefile") for file in list_homologous]

if config['final_fast_tree'] == "yes" or (type(config['final_fast_tree']) == bool and config['final_fast_tree']):
    simulation_output_all_files += [os.path.join(simulation_dir, file + ".fast_tree") for file in list_homologous]

print(simulation_output_all_files)

include: os.path.join(RULES,"1-find_substitution_model.rules")
include: os.path.join(RULES,"2-simulation.rules")
include: os.path.join(RULES,"3-post-processing.rules")

singularity: "unreal_viruses/singularity/mock_v.sif"

rule all:
    input:
        simulation_output_all_files