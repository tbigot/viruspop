import os

#-------------------------------------------------------
# read config files

RULES = os.path.join("rules_simulation")

generated_data_dir = config['generated_data_dir']
simulation_dir = os.path.join(generated_data_dir, config['simulation_folder'])
ancestral_sequence_dir = os.path.join(generated_data_dir, config['ancestral_sequence_folder'])
iqtree_dir = os.path.join(generated_data_dir, config['iqtree_folder'])
fetched_data_dir = os.path.join(generated_data_dir, config['fetched_data_folder'])
cluster_dir = os.path.join(generated_data_dir, config['cluster_folder'])
taxon_file = os.path.join(generated_data_dir, config["list_taxon_file"])

cluster_blast_file = os.path.join(generated_data_dir, "prot.blast.out")
cluster_silix_file = os.path.join(generated_data_dir, "prot.silix.out")

rule clean_all:
    shell:
        """
        rm -rf {ancestral_sequence_dir}
        rm -rf {simulation_dir}
        rm -rf {iqtree_dir}
        rm -rf {fetched_data_dir}
        rm -rf {cluster_dir}
        rm -f {taxon_file}
        rm -f {cluster_blast_file}
        rm -f {cluster_silix_file}
        """

rule clean_simu:
    shell:
        """
        rm -rf {ancestral_sequence_dir}
        rm -rf {simulation_dir}
        rm -rf {iqtree_dir}with_simulation/
        """
