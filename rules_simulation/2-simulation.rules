nb_simulation_per_ancestral_node = config["nb_simulation_per_ancestral_node"]
dist_for_simulations = config["dist_for_simulations"]

ancestral_node_option = "-d " + str(config["dist_for_cluster"])
if config["ancestral_node_file"]:
    ancestral_node_option = "-f " + config["ancestral_node_file"]

dist_variation_number_for_sim = config["dist_variation_number_for_sim"]

rule reverse_translation:
    input:
        fasta_aln_file = f"{iqtree_dir}ancestral/{{cluster}}.aln",
        all_simulation_file = f"{simulation_dir}{{cluster}}.all.aln"
    output:
        nuc_init_file = f"{cluster_dir}{{cluster}}.nuc",
        simu_all_nuc = f"{simulation_dir}{{cluster}}.simu.nuc"
    shell:
        """
        python3 {scripts_dir}get_initial_nuc_sequences.py {fetched_data_dir} {input.fasta_aln_file} {output.nuc_init_file}
        python3 {scripts_dir}reverse_translation.py {input.all_simulation_file} {output.nuc_init_file} {output.simu_all_nuc}
        """


rule recreate_gaps:
    input:
        all_sim_and_init= f"{simulation_dir}{{cluster}}.all_no_del",
        tree_file= f"{iqtree_dir}ancestral/{{cluster}}.aln.nex.treefile"
    output:
        all_aln = f"{simulation_dir}{{cluster}}.all.aln",
        all_unaln = f"{simulation_dir}{{cluster}}.all"
    shell:
        """
        python3 {scripts_dir}generate_deletions.py {input.tree_file} {input.all_sim_and_init} {output.all_aln}
        sed 's/-//g' {output.all_aln} > {output.all_unaln}
        """


rule simulation:
    input:
        fasta_aln_file = f"{iqtree_dir}ancestral/{{cluster}}.aln",
        nexus_file = f"{iqtree_dir}ancestral/{{cluster}}.aln.nex.best_model.nex",
        ancestral_sequence_file = f"{ancestral_sequence_dir}{{cluster}}.ancestral"
    output:
        output_all_simu = temporary(f"{simulation_dir}{{cluster}}.all_no_del.simu"),
        output_all = temporary(f"{simulation_dir}{{cluster}}.all_no_del")
    threads: 8
    shell:
        """
        if [ ! -z "{dist_for_simulations}" -a "{dist_for_simulations}" != " " ]; then
          for dist in {dist_for_simulations} 
          do
            python3 {scripts_dir}iqtree_simulation.py {input.ancestral_sequence_file} {input.nexus_file} {simulation_dir}/{wildcards.cluster}.dist_${{dist}} {threads} -d ${{dist}} 
          done
        else
          python3 {scripts_dir}iqtree_simulation.py {input.ancestral_sequence_file} {input.nexus_file} {simulation_dir}/{wildcards.cluster}.dist_var {threads} --dist_variation_number {dist_variation_number_for_sim}
        fi
        cat "{simulation_dir}{wildcards.cluster}".dist* > "{output.output_all_simu}"
        rm "{simulation_dir}{wildcards.cluster}".dist* 
        cat "{output.output_all_simu}" "{input.fasta_aln_file}" > "{output.output_all}"
        """




rule generate_ancestral_sequences:
    input:
        rate_file=f"{iqtree_dir}ancestral/{{cluster}}.aln.nex.state",
        nexus_file=f"{iqtree_dir}ancestral/{{cluster}}.aln.nex.best_model.nex",
        tree_file=f"{iqtree_dir}ancestral/{{cluster}}.aln.nex.treefile"
    output:
        f"{ancestral_sequence_dir}{{cluster}}.ancestral"
    shell:
        """
        python3 {scripts_dir}create_node_sequences.py {input.rate_file} {input.nexus_file} {input.tree_file} {output} {ancestral_node_option} --no_consensus -n {nb_simulation_per_ancestral_node}
        """