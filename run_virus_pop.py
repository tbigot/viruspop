#!/usr/bin/env python3
import argparse
import functools
import os
import subprocess
import sys

from src.utils_argparse import check_positive, check_is_list, MyFormatter

__version__ = "1.1"

def ask_for_validation():
    while True:
        try:
            ok = input("The project folder already contains results from a run with different parameters.\n"
                       "Are you sure you want to proceed ?"
                       "(To avoid inconsistencies, use the --clean_sim option to clean all simulation related data or "
                       "the --clean_all option to start over.) "
                       "[y/n]")
            if ok.lower() == "y":
                return True
            elif ok.lower() == "n":
                return False
            raise ValueError
        except ValueError:
            print("Sorry, I didn't understand that.")
            continue

def compare_commands(command1, command2):
    args_short = ["-d", '-c', '-l', '-m']
    args_long = ["--rerun-incomplete", "--prepare-data", '--dry-run', "--final_tree", "--cpu", "--local",
                 "--final_fast_tree", "--final_tree", "--blast_visual", "--blast"]

    def simplify(command):
        simp_1 = functools.reduce(lambda com, arg: com.replace(" " + arg, ""), args_long, command)
        simp_2 = functools.reduce(lambda com, arg: com.replace(" " + arg + " ", " ").replace(" " + arg + "\n", "\n"), args_short, simp_1)
        return simp_2
    return simplify(command1) == simplify(command2)

def edit_yaml_config(input_file, output_file, new_parameters_dict: dict):
    with open(input_file) as input:
        data = input.readlines()
    for k, l in enumerate(data):
        if l.strip().split(":")[0].strip() in new_parameters_dict:
            data[k + 1] = "    " + str(new_parameters_dict[l.strip().split(":")[0].strip()]) + "\n"
    if not os.path.isdir(os.path.dirname(output_file)):
        os.makedirs(os.path.dirname(output_file))
    with open(output_file, 'w') as output:
        output.writelines(data)





description = """
EXAMPLE:
    ./run_virus_pop.py group_name Orthopneumovirus -s 20 -t 15 -P Project_Orthopneumovirus_simu
    
    After a run, we recommend the user to check the positions of the internal nodes selected as origins for the simulations.
    You should visual the nodes in this Newick tree: iqtree/ancestral/<protein_cluster>.treefile
    Selected nodes are the prefix of simulation sequence identifiers.
    If you are not satisfied with selected node, you should:
    \t- Use the '--clean_sim' option on your project
    \t- Create a file with the list of your nodes on interest
    \t- Run again Virus Pop on your project and add the '--ancestral_node_file' option 
"""

if __name__ == "__main__":
    ## \cond
    parser = argparse.ArgumentParser(description=description, formatter_class=MyFormatter)
    parser.add_argument("-v", "--version", action='version',
                    version='%(prog)s {version}'.format(version=__version__))
    parser.add_argument("data_type",
                        choices=["group_name", "taxon_file", "genome_directory", "homologous_protein_directory",
                                 "homologous_protein_file"], help="Input data type")
    parser.add_argument("data_info", help="Data info: file name or directory (depending on the data type)")
    parser.add_argument("project_name", help="Project name. All generated files will be put in DATA_generated/<PROJECT_NAME>")

    group_data = parser.add_argument_group("DATASET CONSTRUCTION OPTIONS")
    group_data.add_argument("-t", "--target_n_genome", type=check_positive,
                        help="Target number of genomes to be downloaded per species found (default = 5)")
    group_data.add_argument("-s", "--min_cluster_size", help="minimum size of clusters to process (default = 25)")

    group_simu = parser.add_argument_group("SIMULATION OPTIONS")
    group_simu.add_argument("-f", "--ancestral_node_file",
                        help="Path to file with list of internal node names at which ancestral sequences will be generated"
                             "\nMust be a text file with one node per line. Eg:"
                             "\nNode5"
                             "\nNode88"
                             "\nNode123")
    group_simu.add_argument("--all_ancestral_nodes", action='store_true', help="Generate simulations starting from all ancestral nodes")
    group_simu.add_argument("-n", "--n_simu", type=check_positive,
                        help="number of ancestral sequence per node (default = 10)")
    group_simu.add_argument("-e", "--evolution_distance", help="distance to simulate from each selected ancestral ."
                                                               "\nMust be list within brackets. Eg. \"0.1 0.2 0.3 0.5 0.75 1\""
                                                               "\nBy default, Virus Pop will generate from 0.5 to 2 times the mean distance in the sub-tree.",
                        type=check_is_list)

    group_post = parser.add_argument_group("POST-PROCESSING OPTIONS")
    group_post.add_argument("--final_tree", action='store_true', help="Generate phylogenetic trees with simulated sequences (with IQ-TREE and inferred partition model)")
    group_post.add_argument("--final_fast_tree", action='store_true', help="Generate final phylogenetic trees with simulated sequences (with Fast Tree)")

    group_general = parser.add_argument_group("GENERAL")
    group_general.add_argument("-c", "--cpu", type=check_positive,
                        help="Use at most N CPU cores/jobs in parallel. If N omitted, the limit is set to the number of available CPU cores.")
    group_general.add_argument("-r", "--seed", type=check_positive, help="Seed number for reproducibility of random processes")
    group_general.add_argument("-C", "--clean_all", action='store_true',
                        help="clean all generated files before launching the pipeline")
    group_general.add_argument("-m", "--manually_edited", action='store_true',
                        help="Use this option if you manually edited the configuration file. It will not be overwritten")
    group_general.add_argument("--clean_sim", action='store_true', help="clean simulations only")
    group_general.add_argument("-p", "--print", action='store_true',
                        help="Do not run the pipeline but process the inputs for configuration and show makefile commands")
    group_general.add_argument("--prepare-data", action='store_true', help="Prepare the dataset only (no evolutionary model and no simulation).")

    group_snake = parser.add_argument_group("SNAKEMAKE OPTIONS")
    group_snake.add_argument("--rerun-incomplete", action='store_true', help="Re-generate incomplete files")
    group_snake.add_argument("-d", "--dry-run", action='store_true', help="Dry-run of snakemake pipelines")
    group_snake.add_argument("-l", "--local", action='store_true', help="Run locally (not on container)")

    args_simu = ["clean_sim", "n_simu", "ancestral_node_file", "evolution_distance"]
    args_not_compare = ["rerun_incomplete", "prepare-data", "dry_run", "final_tree", "cpu"]

    args = parser.parse_args()
    project_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "DATA_generated",
                                     args.project_name if args.project_name else "project")
    config_file = os.path.join(project_directory, "config.yaml")
    if not args.manually_edited:
        new_parameters = {"data_type": args.data_type, "data_info": args.data_info}
        if args.project_name:
            new_parameters["generated_data_dir"] = os.path.join("DATA_generated", args.project_name)
        if args.min_cluster_size:
            new_parameters["min_size_cluster"] = args.min_cluster_size
        if args.n_simu:
            new_parameters["nb_simulation_per_ancestral_node"] = args.n_simu
        if args.evolution_distance:
            new_parameters["dist_for_simulations"] = args.evolution_distance
        if args.ancestral_node_file:
            new_parameters["ancestral_node_file"] = args.ancestral_node_file
        if args.all_ancestral_nodes:
            new_parameters["ancestral_node_file"] = 'all'
        if args.target_n_genome:
            new_parameters["nb_init_target"] = str(args.target_n_genome)
        if args.seed:
            new_parameters["seed"] = str(args.seed)
        if args.final_tree:
            new_parameters["generate_final_tree"] = "yes"
        if args.final_fast_tree:
            new_parameters["final_fast_tree"] = "yes"
        edit_yaml_config("src/config_default.yaml", config_file, new_parameters)
    else:
        if not os.path.isfile(config_file):
            raise RuntimeError("There should be a %s file if you are using the '--manually_edited' option" %config_file)

    if args.cpu:
        command_cpu = ["--cores", str(args.cpu)]
    else:
        command_cpu = ["--cores", "all"]
    snakemake_option = ["-p", '--keep-going']
    if args.dry_run:
        snakemake_option += ["--dry-run"]
    if args.rerun_incomplete:
        snakemake_option += ["--rerun-incomplete"]
    if not args.local:
        snakemake_option += ["--use-singularity", "--singularity-args",  '\"-B ../:/virus_pop,${TMPDIR}\"']
    snakemake_1 = ["snakemake"] + command_cpu + snakemake_option + ["-s"] + ["make_clusters.smk"]\
                  + ["--configfile"] + [config_file] + ["--stats"] + [os.path.join(project_directory, "stats1")]
    snakemake_2 = ["snakemake"] + command_cpu + snakemake_option + ["-s"] + ["simulation.smk"]\
                  + ["--configfile"] + [config_file] + ["--stats"] + [os.path.join(project_directory, "stats2")]

    if args.print:
        if not args.data_type in ["homologous_protein_directory", "homologous_protein_file"]:
            print(" ".join(snakemake_1))
        print(" ".join(snakemake_2))
    else:
        if args.clean_all:
            subprocess.run(["snakemake"] + command_cpu + snakemake_option + ["-s", "clean.smk", "-R", "--until", "clean_all", "--configfile"] + [config_file])
        elif args.clean_sim:
            subprocess.run(["snakemake"] + command_cpu + snakemake_option + ["-s", "clean.smk", "-R", "--until", "clean_simu", "--configfile"] + [config_file])
        else:
            command_save_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), "DATA_generated",
                                             args.project_name if args.project_name else "project", "command.save")
            current_command = " ".join(sys.argv) + "\n" + (
                " ".join(snakemake_2) if args.data_type in ["homologous_protein_directory",
                                                            "homologous_protein_file"] else " ".join(
                    snakemake_1) + "\n" + " ".join(snakemake_2))
            if os.path.isfile(command_save_file):
                command_save = open(command_save_file).read()
                # if not compare_commands(command_save, current_command):
                #     if not ask_for_validation():
                #         print('Interrupting')
                #         sys.exit()
            if not os.path.exists(os.path.dirname(command_save_file)):
                os.makedirs(os.path.dirname(command_save_file))
            with open(command_save_file, 'w') as save:
                save.write(current_command)
            if not args.data_type in ["homologous_protein_directory", "homologous_protein_file"]:
                subprocess.run(snakemake_1)
            if not args.prepare_data:
                subprocess.run(snakemake_2)



